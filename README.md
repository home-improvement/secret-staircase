# "Secret" Spiral Staircase

Copyright &copy 2017 Kevin Cole (ubuntourist@hacdc.org) 2017.07.07

As a child, I had this idea for a bookcase that was "secretly" a
spiral staircase leading up to a second floor. Maybe I saw it in
a movie, or maybe I dreamed it up on my own...

Fast-forward to 2017, when a gang of regulars was sitting around
at our local hackerspace, [HacDC](http://hacdc.org/), when one
of our members, Karen, and I got chatting about odd architectural
ideas, and the staircase sprung from my memory.  However, now, I
have the skill to model it in OpenSCAD...

To see it in action, open `staircase.scad` in OpenSCAD, then choose
`View -> Animate`. Set the `FPS` to **30** and the `Steps` to
**720**. (The value for `Steps` will work best as a multiple
of 360. Larger values mean slower animation.)
