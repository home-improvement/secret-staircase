// Spiral staircase / bookshelf animation
// Written by Kevin Cole <dc.loco@gmail.com> 2017.07.07
//
// Based on a childhood fantasy, a "hidden" staircase that is normally
// a bookshelf. Thanks to Karen at HacDC for making me think about
// this again.
//
// NOTE: The subtracted (invisible) objects are larger than necessary, and translated
// so that they render better in preview mode.
//

// Each variable is a list (i.e. array) of random numbers
//
widths  = rands(1, 6, 100);  // (X)
depths  = rands(5, 9, 100);  // (Y)
heights = rands(5, 8, 100);  // (Z)
gaps    = rands(1, 6, 100);  // (X)

reds    = rands(0, 1, 100);
greens  = rands(0, 1, 100);
blues   = rands(0, 1, 100);

module book(slot) {
  union() {
    color([reds[slot], greens[slot], blues[slot]])
    difference() {
      cube([widths[slot], depths[slot], heights[slot]]);
      translate([0.05, 0.05, 0])
      cube([widths[slot] - 0.1, depths[slot] + 0.1, heights[slot] + 1]);
    }
    translate([0.1, 0.04, 0.04])
    color([1, 1, 1])
      cube([widths[slot] - 0.1, depths[slot] - 0.2, heights[slot] - 0.2]);
  }
}

offset = 0;

// DEBUG
//
for (space=[0:20]) {                                  // DEBUG
    translate([offset, 9 - depths[space], 0])         // DEBUG
      book(space);                                    // DEBUG
      offset = offset + widths[space] + gaps[space];  // DEBUG
      echo (offset);                                  // DEBUG
}                                                     // DEBUG
