// Spiral staircase / bookshelf animation
// Written by Kevin Cole <dc.loco@gmail.com> 2017.07.07
//
// Based on a childhood fantasy, a "hidden" staircase that is normally
// a bookshelf. Thanks to Karen at HacDC for making me think about
// this again.
//
// NOTE: The subtracted (invisible) objects are larger than necessary,
// and translated so that they render better in preview mode.
//

module stair() {
difference() {
  union() {
    color([0.5, 0.5, 0.5])
    cylinder(h=10, r=5, $fn=200);
    translate([0, -5, 0.25]) {
      difference() {
        color([0.85, 0.65, 0])
        cube([31, 10, 9.5]);
        translate([0, -5, 0.75])
          color([0.85, 0.65, 0])
          cube([30,  14, 8]);
      }
    }
  }

// TESTING
// offset = 30;
//
// for (space=[0:20]) {                                // TESTING
//   offset = offset - (widths[space] + gaps[space]);  // TESTING
//   translate([offset, (9 - depths[space]), 0])       // TESTING
//   book(space);                                      // TESTING
// }                                                   // TESTING

  translate([0, 0, -2])
    color([0.5, 0.5, 0.5])
    cylinder(h=15, r=4, $fn=200);
  }
}

//
//

stair();
