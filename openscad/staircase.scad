// Spiral staircase / bookshelf animation
// Written by Kevin Cole <dc.loco@gmail.com> 2017.07.07
//
// Based on a childhood fantasy, a "hidden" staircase that is normally
// a bookshelf. Thanks to Karen at HacDC for making me think about
// this again.
//
// To see it in action, open this in OpenSCAD, then choose `View ->
// Animate`. Set the `FPS` to 30 and the `Steps` to 720. (The value
// for `Steps` will work best as a multiple of 360. Larger values mean
// slower animation.)
//

//use <book.scad>  // In case I want to demo filled shelves.

use <stair.scad>   // The journey begins with a single step...

direction =  $t < 0.5 ? $t : 1 - $t;
union() {
  for (next=[0:10]) {
    translate([0, 0, next*10])
    rotate([0, 0, -35*direction*next])
    stair();
  }
}
